# Trios

We have measured gene expression via RNA-seq and DNA methylation via Illumina's 450K array in the Yoruba trios.

### Steps for methylation data:

* Probe pre-processing (Nick has scripts: home/nbanovich/methyQTL/READ_ME.txt)
  * Remove probes that do not map to genome (methylated or unmethylated)
  * Remove probes that contain SNPs
* Normalize probe intensities
  * Within arrays using SWAN
  * Across individuals
* Quality control
  * PCA plots against known confounders
